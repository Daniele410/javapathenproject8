package tourGuide.controller;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import jakarta.validation.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.exception.DataNotFoundException;
import tourGuide.service.ITourGuideService;
import tourGuide.service.IUserService;
import tripPricer.Provider;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
public class TourGuideController {
    private final Logger logger = LoggerFactory.getLogger(TourGuideController.class);

    ITourGuideService tourGuideService;

    IUserService userService;

    public TourGuideController(ITourGuideService tourGuideService, IUserService userService) {
        this.tourGuideService = tourGuideService;
        this.userService = userService;
    }


    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    /**
     * controller to get all users
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @RequestMapping("/getAllUser")
    public ResponseEntity<List<User>> getAllUsers() throws ExecutionException, InterruptedException {
        List<User> users = userService.getAllUsers();
        logger.info("@RequestMapping(\"/getAllUser\")");
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    /**
     * controller to get User By UserName
     * @param userName
     * @return
     * @throws DataNotFoundException
     */
    @RequestMapping("/getUser")
    public ResponseEntity<User> getUserByUserName(@RequestParam String userName) throws DataNotFoundException {
        User user = userService.getUser(userName);
        logger.info("@RequestMapping(\"/getUser/{userName}\")");
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    /**
     * controller to get Location
     * @param userName
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @RequestMapping("/getLocation")
    public Location getLocation(@RequestParam String userName) throws ExecutionException, InterruptedException {
        User user = userService.getUser(userName);
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(user).get();
        logger.info("@RequestMapping(\"/getLocation/{userName}\")");
        return visitedLocation.location;
    }

    /**
     * controller to get Nearby Attractions
     * @param userName
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    //  TODO: Change this method to no longer return a List of Attractions.
    //  Instead: Get the closest five tourist attractions to the user - no matter how far away they are.
    //  Return a new JSON object that contains:
    //  Name of Tourist attraction,
    //  Tourist attractions lat/long,
    //  The user's location lat/long,
    //  The distance in miles between the user's location and each of the attractions.
    //  The reward points for visiting each Attraction.
    //  Note: Attraction reward points can be gathered from RewardsCentral
    @RequestMapping("/getNearByAttractions")
    public ResponseEntity getNearbyAttractions(@RequestParam String userName)  {
        logger.info("@RequestMapping(\"/getNearbyAttractions/{userName}\")");
        return new ResponseEntity<>(tourGuideService.getNearbyAttractions(userName), HttpStatus.OK);
    }

    /**
     * controller to get Rewards
     * @param userName
     * @return
     */
    @RequestMapping("/getRewards")
    public ResponseEntity<List<UserReward>> getRewards(@RequestParam String userName) {
        logger.info("@RequestMapping(\"/getRewards/{userName}\")");
        return new ResponseEntity<>(userService.getUserRewards(userService.getUser(userName)), HttpStatus.OK);
    }

    /**
     * @return controller to get all current locations
     * @throws ExecutionException
     * @throws InterruptedException
     */
    // TODO: Get a list of every user's most recent location as JSON
    //- Note: does not use gpsUtil to query for their current location,
    //        but rather gathers the user's current location from their stored location history.
    //
    // Return object should be the just a JSON mapping of userId to Locations similar to:
    //     {
    //        "019b04a9-067a-4c76-8817-ee75088c3822": {"longitude":-48.188821,"latitude":74.84371}
    //        ...
    //     }
    @RequestMapping("/getAllCurrentLocations")
    public Map<String, Location> getAllCurrentLocations() {
        Map<String, Location> locations = tourGuideService.getAllCurrentLocations();
        logger.info("@RequestMapping(\"/getAllCurrentLocations\")");
        return new ResponseEntity<>(locations, HttpStatus.OK).getBody();
    }

    /**
     * controller to get user Trip Deals
     * @param userName
     * @return
     */
    @RequestMapping("/getTripDeals")
    public ResponseEntity<List<Provider>> getTripDeals(@RequestParam String userName) {
        List<Provider> providers = tourGuideService.getTripDeals(userService.getUser(userName));
        logger.info("@RequestMapping(\"/getTripDeals/{userName}\")");
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }

    /**
     * controller to get user preferences
     * @param userName
     * @return
     */
    //controller pour modifier les préférences utilisateurs
    @RequestMapping("/userPreference")
    public ResponseEntity getUserPreference(@RequestParam String userName) {
        UserPreferencesDto userPreference = userService.getUserPreferences(userName);
        logger.info("@RequestMapping(\"/userPreference/{userName}\")");
        return new ResponseEntity<>(userPreference, HttpStatus.OK);
    }

    /**
     * controller to change user preferences
     * @param userName
     * @param userPreferencesDto
     * @return
     */
    @PutMapping("/userPreference")
    public ResponseEntity updateUserPreference(@RequestParam @NotBlank final String userName,@RequestBody final UserPreferencesDto userPreferencesDto) {
        UserPreferencesDto preferences = tourGuideService.updatePreferences(userName, userPreferencesDto);
        logger.info("@PutMapping(\"/userPreference/{userName}\")");
        return new ResponseEntity<>(preferences,HttpStatus.OK);
    }


}