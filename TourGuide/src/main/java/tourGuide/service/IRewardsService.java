package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import tourGuide.model.user.User;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface IRewardsService {

    CompletableFuture<Void> calculateRewards(User user);

    boolean isWithinAttractionProximity(Attraction attraction, Location location);

    double getDistance(Location loc1, Location loc2);

    public Integer getAttractionRewardPoints(UUID attractionId, UUID userId);

    public void setProximityBuffer(int proximityBuffer);

    public void setDefaultProximityBuffer();

    public double distanceBetweenAttractionAndVisitedLocation(Attraction attraction, Location location);



    }
