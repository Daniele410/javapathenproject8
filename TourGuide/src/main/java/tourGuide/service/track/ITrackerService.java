package tourGuide.service.track;

import java.util.concurrent.ExecutionException;

public interface ITrackerService {

    void stopTracking();

    void run()throws ExecutionException, InterruptedException;


}
