package tourGuide.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import tourGuide.service.ITripPricerService;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.util.List;
import java.util.UUID;

/**
 *  contain all business service methods for TripPricerService
 */
@Service
public class TripPricerService implements ITripPricerService {

    /**
     * SLF4J LOGGER instance.
     */
    private static final Logger logger =  LogManager.getLogger("TripPricerService");


    /**
     * TripPricer instance
     */
    private final TripPricer tripPricer;

    public TripPricerService(TripPricer tripPricer) {
        this.tripPricer = tripPricer;
    }


    /**
     * @param apiKey
     * @param attractionId
     * @param adults
     * @param children
     * @param nightsStay
     * @param rewardsPoints
     * @return Filter prices based on user price preferences
     */
    public List<Provider> getPrice(String apiKey, UUID attractionId, int adults, int children, int nightsStay, int rewardsPoints) {

        return tripPricer.getPrice(apiKey, attractionId, adults, children, nightsStay, rewardsPoints);
    }

    /**
     * @param apiKey
     * @param adults
     * @return get provider name
     */
    public String getProviderName(String apiKey, int adults){
        return tripPricer.getProviderName(apiKey,adults);
    }



}
