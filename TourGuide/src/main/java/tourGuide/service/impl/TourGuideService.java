package tourGuide.service.impl;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import tourGuide.model.dto.AttractionDto;
import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tourGuide.model.user.UserPreferences;
import tourGuide.model.user.UserReward;
import tourGuide.service.*;
import tourGuide.service.track.ITrackerService;
import tourGuide.service.track.TrackerService;
import tripPricer.Provider;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
/**
 * contain all business service methods for TourGuideService
 */
@Service
public class TourGuideService implements ITourGuideService {
    /**
     * Logger instance.
     */
    private Logger logger = LoggerFactory.getLogger(TourGuideService.class);

    /**
     * IGpsUtilService instance.
     */
    private final IGpsUtilService gpsUtil;

    /**
     * IRewardsService instance.
     */
    private final IRewardsService rewardsService;

    /**
     * ITripPricerService instance.
     */
    private final ITripPricerService tripPricerService;

    /**
     * IUserService instance.
     */
    private final IUserService userService;

    /**
     * ITrackerService instance.
     */
    public final ITrackerService tracker;

    /**
     * Activate test mode
     */
    boolean testMode = true;

    private static final String tripPricerApiKey = "test-server-api-key";

    public TourGuideService(IGpsUtilService gpsUtil, IRewardsService rewardsService, ITripPricerService tripPricerService, IUserService userService) {
        this.gpsUtil = gpsUtil;
        this.rewardsService = rewardsService;
        this.tripPricerService = tripPricerService;
        this.userService = userService;

        if (testMode) {
            logger.info("TestMode enabled");
            logger.debug("Initializing users");
            userService.initializeInternalUsers();
            logger.debug("Finished initializing users");
        }
        tracker = new TrackerService(this, userService);
        addShutDownHook();

    }


    /**
     * @param user
     * @return get Trip Deals
     */
    @Override
    public List<Provider> getTripDeals(User user){
        int cumulativeRewardPoints = user.getUserRewards().stream().mapToInt(userReward -> userReward.getRewardPoints()).sum();

        CompletableFuture<List<Provider>> futureProvider = CompletableFuture.supplyAsync(()->
                tripPricerService.getPrice(tripPricerApiKey,user.getUserId(),user.getUserPreferences().getNumberOfAdults()
                ,user.getUserPreferences().getNumberOfChildren(),user.getUserPreferences().getTripDuration(),
                cumulativeRewardPoints));
        futureProvider.thenApply(providers -> {
            user.setTripDeals(providers);

            logger.debug("getTripDeals: {}userName", user.getUserName());

            return providers;
        });

        return futureProvider.join();
    }


    /**
     * get five tourist attractions closest to the user's last visited location.
     * @param userName
     * @return
     */
    @Override
    public List<AttractionDto> getNearbyAttractions(String userName) {
        User user = userService.getUser(userName);

        CompletableFuture<List<AttractionDto>> future = CompletableFuture.supplyAsync(() -> gpsUtil.getAttractions().stream()
                .map(a -> {
                    double distance = rewardsService.distanceBetweenAttractionAndVisitedLocation(a, user.getLastVisitedLocation().location);
                    return new AttractionDto(a.attractionId, a.attractionName, new Location(a.latitude, a.longitude),
                            new Location(user.getLastVisitedLocation().location.latitude,
                                    user.getLastVisitedLocation().location.longitude), 0, distance);
                }).sorted(Comparator.comparing(AttractionDto::getDistance))
                .limit(5).collect(Collectors.toList()));

        List<AttractionDto> attractions = future.join();
        attractions.forEach(r -> rewardsService.getAttractionRewardPoints(r.getAttractionId(), user.getUserId()));

        return attractions;
    }



    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> tracker.stopTracking()));
    }

    /**
     * track a user's location through GPS
     * @param user
     * @return
     */
    @Async
    public CompletableFuture<VisitedLocation> trackUserLocation(User user) {
        logger.debug(user.getUserName());
        return gpsUtil.getUserLocation(user.getUserId()).thenApply(visitedLocation -> {
            user.addToVisitedLocations(visitedLocation);
            rewardsService.calculateRewards(user);
            return CompletableFuture.completedFuture(visitedLocation).join();
        });
    }

    /**
     * get User Location checks if the user already has a registered location
     * and returns the registered location or draws a new one.
     * @param user
     * @return
     */
    @Async
    public CompletableFuture<VisitedLocation> getUserLocation(User user) {
        logger.debug("get user: {} location", user.getUserName());
        if (user.getVisitedLocations().size() > 0) {
            return CompletableFuture.completedFuture(user.getLastVisitedLocation());
        }
        return trackUserLocation(user);
    }


    @Override
    public Map<String, Location> getAllCurrentLocations() {
        logger.debug("get All Users Location");

        CompletableFuture<Map<String , Location>> future = CompletableFuture.supplyAsync(() -> userService.getAllUsers().stream()
                .collect(Collectors.
                        toMap(user -> user.getUserId().toString(), user -> getUserLocation(user).join().location)));

        return future.join();
    }


    /**
     * a method that updates user preferences.
     * @param userName
     * @param userPreferenceDto
     * @return
     */
    public UserPreferencesDto updatePreferences(String userName, UserPreferencesDto userPreferenceDto) {
        User user = userService.getUser(userName);

        UserPreferences preferences = user.getUserPreferences();
        preferences.setAttractionProximity(userPreferenceDto.getAttractionProximity());
        preferences.setLowerPricePoint(Money.of(userPreferenceDto.getLowerPricePoint(), preferences.getCurrency()));
        preferences.setHighPricePoint(Money.of(userPreferenceDto.getHighPricePoint(), preferences.getCurrency()));
        preferences.setTripDuration(userPreferenceDto.getTripDuration());
        preferences.setTicketQuantity(userPreferenceDto.getTicketQuantity());
        preferences.setNumberOfAdults(userPreferenceDto.getNumberOfAdults());
        preferences.setNumberOfChildren(userPreferenceDto.getNumberOfChildren());

        logger.debug("get updatePreferences: {} userPreferenceDto",userPreferenceDto);

        return userPreferenceDto;
    }

}
