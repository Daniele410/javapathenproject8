package tourGuide.service.impl;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.service.IGpsUtilService;
import tourGuide.service.IRewardsService;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *  contain all business service methods for RewardsService
 */
@Service
@EnableAsync
public class RewardsService implements IRewardsService {

    /**
     * SLF4J LOGGER instance.
     */
    private Logger logger = LoggerFactory.getLogger(RewardsService.class);

    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

    // proximity in miles
    private int defaultProximityBuffer = 10;
    private int proximityBuffer = defaultProximityBuffer;
    private int attractionProximityRange = 200;

    /**
     * IGpsUtilService instance.
     */
    private static IGpsUtilService gpsUtil;

    /**
     * RewardCentral instance
     */
    private static RewardCentral rewardCentral;

    @Autowired
    public RewardsService(IGpsUtilService gpsUtil, RewardCentral rewardCentral) {
        this.gpsUtil = gpsUtil;
        this.rewardCentral = rewardCentral;
    }

    private final ExecutorService executorService = Executors.newFixedThreadPool(200);


    public Integer getAttractionRewardPoints(UUID attractionId, UUID userId) {
        return rewardCentral.getAttractionRewardPoints(attractionId, userId);
    }

    public void setDefaultProximityBuffer() {
        proximityBuffer = defaultProximityBuffer;
    }

    /**
     * Calculate the reward for a user
     * @param user
     * @return
     */
    @Async
    public CompletableFuture<Void> calculateRewards(User user) {

        logger.debug(" calculating reward for user: {} ", user.getUserName());
        final List<VisitedLocation> userLocations = user.getVisitedLocations();
        final List<UserReward> userRewards = user.getUserRewards();
        final List<Attraction> attractions = gpsUtil.getAttractions();
        return CompletableFuture.runAsync(() -> {
            for (VisitedLocation visitedLocation : userLocations) {
                attractions.stream()
                        .filter(attraction -> nearAttraction(visitedLocation, attraction))
                        .forEach(attraction -> {
                            if (userRewards.stream()
                                    .noneMatch(r -> r.getAttraction().attractionName
                                            .equals(attraction.attractionName))) {
                                user.addUserReward(new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));
                            }
                        });
            }
        }, executorService);

    }

    public void setProximityBuffer(int proximityBuffer) {
        this.proximityBuffer = proximityBuffer;
    }

    public void setDefaultProximityBuffer(int proximityBuffer) {
        this.defaultProximityBuffer = proximityBuffer;

    }

    public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
        return getDistance(attraction, location) > attractionProximityRange ? false : true;
    }

    /**
     * @param attraction
     * @param location
     * @return returns distance between attraction and visited location
     */
    public double distanceBetweenAttractionAndVisitedLocation(Attraction attraction, Location location) {
        return getDistance(attraction, location);
    }


    /**
     * @param visitedLocation
     * @param attraction
     * @return returns true with the distance near attraction if major of proximity buffer
     */
    private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
        return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
    }


    /**
     * @param attraction
     * @param user
     * @return get reward points
     */
    private int getRewardPoints(Attraction attraction, User user) {
        return rewardCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
    }

    public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
        return statuteMiles;
    }


}
