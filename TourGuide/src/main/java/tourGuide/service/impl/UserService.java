package tourGuide.service.impl;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import tourGuide.exception.DataNotFoundException;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tourGuide.model.user.UserPreferences;
import tourGuide.model.user.UserReward;
import tourGuide.repository.UserRepository;
import tourGuide.service.IUserService;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

/**
 * contain all business service methods for UserService
 */
@Service
public class UserService implements IUserService {

    /**
     * SLF4J LOGGER instance.
     */
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * UserRepository instance.
     */
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;

    }

    /**
     * Initialize internal Users
     */
    @PostConstruct
    public void postConstructInitializeUsers() {
        this.initializeInternalUsers();
    }

    /**
     * @param user
     * @return get user rewards
     */
    @Override
    public List<UserReward> getUserRewards(User user) {
        logger.info("Get User Rewards");
        CompletableFuture<List<UserReward>> futureUser = CompletableFuture.supplyAsync(()->
                userRepository.getUserByUserName(user.getUserName()).getUserRewards());

        return futureUser.join();
    }


    /**
     * @param userName
     * @return Get user by userName
     * @throws DataNotFoundException
     */
    @Override
    public User getUser(String userName) throws DataNotFoundException {
        logger.info("Get User");
        User user = userRepository.getUserByUserName(userName);

        if (user != null) {
            return user;
        }
        throw new DataNotFoundException(
                "Not Found",
                "User is unknown: " + userName,
                HttpStatus.NOT_FOUND);

    }

    /**
     * Get all users in userRepository
     * @return
     */
    @Override
    public List<User> getAllUsers() throws NullPointerException {
        logger.info("Get All Users");
        return  userRepository.getAllUsers();
    }

    /** add user in the repository
     * @param user
     */
    @Override
    public void addUser(User user) throws DataNotFoundException{
        List<User> users = userRepository.getAllUsers();
        if (!users.contains(user)) {
            logger.info("Add User {}", user.getUserName());
            userRepository.addUser(user.getUserName(), user);
        }else {
            throw new DataNotFoundException(
                    "User already",
                    "User already in dataBase: " + user.getUserName(),
                    HttpStatus.CONFLICT);
        }

    }


    /**
     * Delete all users
     */
    @Override
    public void deleteAllUser() {
        logger.info("Delete all User");
        userRepository.deleteAllUser();
    }


    /**
     * @param userName
     * @return Preference of user
     */
    @Override
    public UserPreferencesDto getUserPreferences(String userName) {
        User user = getUser(userName);
        UserPreferences userPreferences = user.getUserPreferences();
        logger.info("Get UserPreferences {}", userName);
        return new UserPreferencesDto(userPreferences.getAttractionProximity(),
                userPreferences.getLowerPricePoint().getNumber().intValue(),
                userPreferences.getHighPricePoint().getNumber().intValue(),
                userPreferences.getTripDuration(), userPreferences.getTripDuration(),
                userPreferences.getNumberOfAdults(), userPreferences.getNumberOfAdults());
    }


    /**********************************************************************************
     *
     * Methods Below: For Internal Testing
     *
     **********************************************************************************/
    private static final String tripPricerApiKey = "test-server-api-key";

//     Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory

    public void initializeInternalUsers() {
        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            userRepository.addUser(userName, user);
//            internalUserMap.put(userName, user);
        });
        logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
    }


    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach(i -> {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
        });
    }

    private double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

}
