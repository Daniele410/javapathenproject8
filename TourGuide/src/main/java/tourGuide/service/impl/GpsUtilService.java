package tourGuide.service.impl;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import tourGuide.repository.GpsUtilRepository;
import tourGuide.service.IGpsUtilService;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * contain all business service methods for GpsUtilService
 */
@Service
public class GpsUtilService implements IGpsUtilService {

    /**
     * GpsUtilRepository instance
     */
    public final GpsUtilRepository gpsUtilRepository;

    /**
     * creating an ExecutorService object that can be used to execute tasks asynchronously and in parallel
     */
    private final ExecutorService executorService = Executors.newFixedThreadPool(250);

    @Autowired
    public GpsUtilService(GpsUtilRepository gpsUtilRepository) {
        this.gpsUtilRepository = gpsUtilRepository;
    }


    /**
     * @param userId
     * @return
     */
    @Async
    public CompletableFuture<VisitedLocation> getUserLocation(UUID userId) {
        return CompletableFuture.supplyAsync(() -> gpsUtilRepository.getUserLocation(userId), executorService);
    }

    public List<Attraction> getAttractions() {
        return gpsUtilRepository.getAttractions();
    }


}
