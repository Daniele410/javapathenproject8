package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface IGpsUtilService {

    public CompletableFuture<VisitedLocation> getUserLocation(UUID userId);

    public List<Attraction> getAttractions();
}
