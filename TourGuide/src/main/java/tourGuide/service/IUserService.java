package tourGuide.service;

import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.exception.DataNotFoundException;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface IUserService {

    List<UserReward> getUserRewards(User user);

    User getUser(String userName) throws DataNotFoundException;

    List<User> getAllUsers() throws  NullPointerException;

    void addUser(User user) throws DataNotFoundException;

    void deleteAllUser();

    UserPreferencesDto getUserPreferences(String userName);

    void initializeInternalUsers();


}
