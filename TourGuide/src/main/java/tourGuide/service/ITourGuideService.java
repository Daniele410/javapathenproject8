package tourGuide.service;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.model.dto.AttractionDto;
import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tripPricer.Provider;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface ITourGuideService {

    List<Provider> getTripDeals(User user);

    public CompletableFuture<VisitedLocation> trackUserLocation(User user);

    Map<String, Location> getAllCurrentLocations();

    CompletableFuture<VisitedLocation> getUserLocation(User user);

    public UserPreferencesDto updatePreferences(String userName, UserPreferencesDto userPreferenceDto);

    List<AttractionDto> getNearbyAttractions(String userName);
}
