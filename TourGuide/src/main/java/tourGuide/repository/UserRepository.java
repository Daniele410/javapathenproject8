package tourGuide.repository;

import org.springframework.stereotype.Repository;
import tourGuide.model.user.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class UserRepository {

    private final Map<String, User> internalUserMap = new HashMap<>();

    public List<User> getAllUsers() {
        return this.internalUserMap.values().stream().collect(Collectors.toList());
    }

    public User getUserByUserName(String userName) {
        return this.internalUserMap.get(userName);
    }

    public void addUser(String userName, User user) {
        this.internalUserMap.put(userName, user);
    }

//    public void updateUser(String userName, User user) {
//        this.internalUserMap.replace(userName, user);
//    }
//
//    public void removeUser(String userName, User user) {
//        this.internalUserMap.remove(userName, user);
//    }

    public void deleteAllUser() {
        this.internalUserMap.clear();
    }


}
