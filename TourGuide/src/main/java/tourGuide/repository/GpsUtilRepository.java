package tourGuide.repository;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class GpsUtilRepository {

    private final GpsUtil gpsUtil;

    private List<Attraction> attractions = new ArrayList<>();

    @Autowired
    public GpsUtilRepository(GpsUtil gpsUtil) {
        this.gpsUtil = gpsUtil;
        attractions  = gpsUtil.getAttractions();
    }


    public VisitedLocation getUserLocation(UUID userId) {
        return gpsUtil.getUserLocation(userId);
    }

    public List<Attraction> getAttractions() {
        return attractions;


    }


}
