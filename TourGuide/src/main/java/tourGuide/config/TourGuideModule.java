package tourGuide.config;

import gpsUtil.GpsUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import rewardCentral.RewardCentral;
import tripPricer.TripPricer;

/**
 * configuration of the application's TourGuideModule
 */
@Configuration
@EnableAsync
public class TourGuideModule {

    /**
     * instance of gpsUtil
     */
    @Bean
    public GpsUtil getGpsUtil() {
        return new GpsUtil();
    }

    /**
     * instance of tripPricer
     */
    @Bean
    public TripPricer tripPricer() {
        return new TripPricer();
    }

    /**
     * instance of RewardCentral
     */
    @Bean
    public RewardCentral getRewardCentral() {
        return new RewardCentral();
    }


}
