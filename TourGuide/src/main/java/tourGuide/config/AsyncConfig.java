package tourGuide.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * this class is a configuration file for using asynchronous tasks in Spring Framework.
 */
@Configuration
@EnableAsync
public class AsyncConfig {

    @Bean(name = "taskExecutor")
    public Executor taskExecutor(){
        ThreadPoolTaskExecutor executor= new ThreadPoolTaskExecutor();
        //maximum number of executor threads,
        executor.setCorePoolSize(1000);
        executor.setQueueCapacity(100000);
        executor.setThreadNamePrefix("userThread-");
        executor.initialize();

        return executor;
    }
}
