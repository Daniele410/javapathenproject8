package tourGuide.controller;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tourGuide.service.*;
import tourGuide.service.impl.TourGuideService;

import java.util.Locale;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@SpringBootTest
class TourGuideControllerTest {

    @Autowired
    private IGpsUtilService gpsUtil;
    @Autowired
    private IUserService userService;
    @Autowired
    private ITripPricerService tripPricerService;
    @Autowired
    private IRewardsService rewardsService;

    private ITourGuideService tourGuideService;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        userService.deleteAllUser();
        InternalTestHelper.setInternalUserNumber(100);
        tourGuideService = new TourGuideService(gpsUtil, rewardsService, tripPricerService, userService);
        User user = userService.getUser("internalUser23");
    }

    @Test
    void index() throws Exception {

        //Given //When
        mockMvc.perform(get("/")
                        .contentType(MediaType.APPLICATION_JSON))
                //Then
                .andExpect(status().isOk()).andExpect(content().string(containsString(
                        "Greetings from TourGuide!")));
    }

    @Test
    void getAllUsers() throws Exception {

        //Given
        mockMvc.perform(get("/getAllUser")
                        .contentType(MediaType.APPLICATION_JSON))

                //When //Then
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("internalUser23")));


    }

    @Test
    void getUserByUserName() throws Exception {
        //Given
        User user = userService.getUser("internalUser23");
        mockMvc.perform(get("/getUser")
                        .contentType(MediaType.APPLICATION_JSON)

                        //When
                        .param("userName", user.getUserName()))
                //Then
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("internalUser23")));
    }

    @Test
    void getLocation() throws Exception {
        //Given
        User user = userService.getUser("internalUser23");
        mockMvc.perform(get("/getLocation")
                        .contentType(MediaType.APPLICATION_JSON)

                        //When
                        .param("userName", user.getUserName()))
                //Then
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("latitude")))
                .andExpect(content().string(containsString("longitude")));
    }

    @Test
    void getNearbyAttractions() throws Exception {
        //Given
        User user = userService.getUser("internalUser23");
        mockMvc.perform(get("/getNearByAttractions")
                        .contentType(MediaType.APPLICATION_JSON)

                        //When
                        .param("userName", user.getUserName()))
                //Then
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("latitude")))
                .andExpect(content().string(containsString("longitude")));

    }

    @Test
    void getRewards() throws Exception {
        //Given
        User user = userService.getUser("internalUser23");
        mockMvc.perform(get("/getRewards")
                        .contentType(MediaType.APPLICATION_JSON)

                        //When
                        .param("userName", user.getUserName()))
                //Then
                .andExpect(status().isOk());
    }

    @Test
    void getAllCurrentLocations() throws Exception {
        //Given
        mockMvc.perform(get("/getAllCurrentLocations")
                        .contentType(MediaType.APPLICATION_JSON))

                //When //Then
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("latitude")))
                .andExpect(content().string(containsString("longitude")));

    }

    @Test
    void getTripDeals() throws Exception {
        //Given
        User user = userService.getUser("internalUser23");
        mockMvc.perform(get("/getTripDeals")
                        .contentType(MediaType.APPLICATION_JSON)

                        //When
                        .param("userName", user.getUserName()))
                //Then
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("name")))
                .andExpect(content().string(containsString("price")))
                .andExpect(content().string(containsString("tripId")));

    }

    @Test
    void getUserPreference() throws Exception {
        //Given
        User user = userService.getUser("internalUser23");
        mockMvc.perform(get("/userPreference")
                        .contentType(MediaType.APPLICATION_JSON)

                        //When
                        .param("userName", user.getUserName()))
                //Then
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("attractionProximity")))
                .andExpect(content().string(containsString("lowerPricePoint")))
                .andExpect(content().string(containsString("highPricePoint")))
                .andExpect(content().string(containsString("tripDuration")))
                .andExpect(content().string(containsString("ticketQuantity")))
                .andExpect(content().string(containsString("numberOfAdults")))
                .andExpect(content().string(containsString("numberOfChildren")));
    }

    @Test
    void updateUserPreference() throws Exception {
        //Given
        User user = userService.getUser("internalUser23");
        UserPreferencesDto userPreferencesDto = new UserPreferencesDto(2147483647, 500, 1000, 1, 2, 2, 3);
        JSONObject json = new JSONObject();
        json.put("attractionProximity", userPreferencesDto.getAttractionProximity());
        json.put("lowerPricePoint", userPreferencesDto.getLowerPricePoint());
        json.put("highPricePoint", userPreferencesDto.getHighPricePoint());
        json.put("tripDuration", userPreferencesDto.getTripDuration());
        json.put("ticketQuantity", userPreferencesDto.getTicketQuantity());
        json.put("numberOfAdults", userPreferencesDto.getNumberOfAdults());
        json.put("numberOfChildren", userPreferencesDto.getNumberOfChildren());
        mockMvc.perform(put("/userPreference")

                        //When
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("userName", user.getUserName())
                        .content(json.toString()))

                //Then
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("attractionProximity")))
                .andExpect(content().string(containsString("lowerPricePoint")))
                .andExpect(content().string(containsString("highPricePoint")))
                .andExpect(content().string(containsString("tripDuration")))
                .andExpect(content().string(containsString("ticketQuantity")))
                .andExpect(content().string(containsString("numberOfAdults")))
                .andExpect(content().string(containsString("numberOfChildren")));
    }
}