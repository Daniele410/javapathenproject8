package tourGuide.service.Integration;

import gpsUtil.location.Attraction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.user.User;
import tourGuide.service.IGpsUtilService;
import tourGuide.service.IRewardsService;
import tourGuide.service.IUserService;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
@SpringBootTest
public class TestRewardsService {

    @Autowired
    IRewardsService rewardsService;
    @Autowired
    IGpsUtilService gpsUtilService;
    @Autowired
    IUserService userService;

    private Attraction attraction;




    @BeforeEach
    public void init() {
        userService.deleteAllUser();
        InternalTestHelper.setInternalUserNumber(100);
        attraction = gpsUtilService.getAttractions().get(1);

    }


    @Test
    public void calculateReward() throws InterruptedException, ExecutionException {
       //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        userService.addUser(user);
        rewardsService.setProximityBuffer(10000);
        boolean userInlist = userService.getAllUsers().contains(user);

        //When
        rewardsService.calculateRewards(userService.getUser(user.getUserName()));

        user.getUserRewards().forEach(r -> System.out.println(r.getRewardPoints()));
        System.out.println(user.getUserRewards().size());

        //Then
        assertTrue(userInlist);

    }


    @Test
    public void isWithinAttractionProximity() {
        //Given //When // Then
        assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
    }


}
