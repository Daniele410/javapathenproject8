package tourGuide.service.Integration;

import ch.qos.logback.core.CoreConstants;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import tourGuide.exception.DataNotFoundException;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.dto.AttractionDto;
import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tourGuide.model.user.UserPreferences;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.service.impl.TourGuideService;
import tourGuide.service.track.ITrackerService;
import tripPricer.Provider;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@SpringBootTest
public class TestTourGuideService {

    @Autowired
    IUserService userService;

    @Autowired
    IRewardsService rewardsService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    IGpsUtilService gpsUtil;

    @Autowired
    ITripPricerService tripPricerService;

    @Autowired
    ITourGuideService tourGuideService;

    @Autowired
    ITrackerService tracker;

    @BeforeEach
    public void init() {
        userService.deleteAllUser();
        InternalTestHelper.setInternalUserNumber(100);
        tourGuideService = new TourGuideService(gpsUtil, rewardsService, tripPricerService, userService);

    }

    @Test
    public void getTripDeals() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        userService.addUser(user);
        List<Provider> providers = tourGuideService.getTripDeals(user);

        //When
        tourGuideService.getTripDeals(user);

        //Then
        assertEquals(5, providers.size());
    }


    @Test
    public void getNearbyAttractions() throws DataNotFoundException {

        //Given
        User user = userService.getAllUsers().get(0);

        //When
        List<AttractionDto> attractionDtos = tourGuideService.getNearbyAttractions(user.getUserName());

        //Assert
        assertEquals(5, attractionDtos.size());
    }


    @Test
    public void getUserLocation() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        userService.addUser(user);

        //When
        CompletableFuture<VisitedLocation> location = tourGuideService.getUserLocation(user);

        //Then
        assertNotNull(location);

    }

    @Test
    public void trackUserLocation() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        userService.addUser(user);

        //When
        CompletableFuture<VisitedLocation> location = tourGuideService.trackUserLocation(user);

        //Then
        assertNotNull(location);

    }

    @Test
    public void getAllCurrentLocations()  {
        //Given //When
        Map<String, Location> allUserLocation = tourGuideService.getAllCurrentLocations();

        //Then
        assertNotNull(allUserLocation);
        assertEquals(100, allUserLocation.size());

    }

    @Test
    public void updatePreferences() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        user.setUserPreferences(new UserPreferences());
        userRepository.addUser(user.getUserName(),user);
        UserPreferencesDto userPreferenceDto = new UserPreferencesDto();
        userPreferenceDto.setAttractionProximity(10);
        userPreferenceDto.setLowerPricePoint(20);
        userPreferenceDto.setHighPricePoint(50);
        userPreferenceDto.setTripDuration(2);
        userPreferenceDto.setTicketQuantity(3);
        userPreferenceDto.setNumberOfAdults(2);
        userPreferenceDto.setNumberOfChildren(5);

        // When
        UserPreferencesDto userPreferences = tourGuideService.updatePreferences(user.getUserName(), userPreferenceDto);

        //Then
        assertEquals(10, userPreferences.getAttractionProximity());
        assertEquals(20, userPreferences.getLowerPricePoint());
        assertEquals(50, userPreferences.getHighPricePoint());
        assertEquals(2, userPreferences.getTripDuration());
        assertEquals(3, userPreferences.getTicketQuantity());
        assertEquals(2, userPreferences.getNumberOfAdults());
        assertEquals(5, userPreferences.getNumberOfChildren());


    }


}
