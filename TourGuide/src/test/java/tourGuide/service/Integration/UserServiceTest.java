package tourGuide.service.Integration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.exception.DataNotFoundException;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.service.impl.TourGuideService;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
class UserServiceTest {

    @Autowired
    IUserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ITourGuideService tourGuideService;

    @Autowired
    IGpsUtilService gpsUtil;
    @Autowired
    IRewardsService rewardsService;
    @Autowired
    ITripPricerService tripPricerService;

    

    @BeforeEach
    public void init()  {
        userService.deleteAllUser();
        InternalTestHelper.setInternalUserNumber(100);
        tourGuideService = new TourGuideService(gpsUtil, rewardsService, tripPricerService, userService );

    }

    @Test
    public void addUser() throws ExecutionException {
        //Given
        User user = new User(UUID.randomUUID(), "Gin", "123", "gin@tonic.com");
        User user2 = new User(UUID.randomUUID(), "Mario", "064", "gin1@tonic.com");
        List<User> users = userService.getAllUsers();

        //When
        userService.addUser(user);
        userService.addUser(user2);

        User retrivedUser = userService.getUser(user.getUserName());
        User retrivedUser2 = userService.getUser(user2.getUserName());


        //Then
        assertEquals(user, retrivedUser);
        assertEquals(user2, retrivedUser2);
    }

    @Test
    public void getAllUsers() throws ExecutionException, InterruptedException {
        //Given
        User user = new User(UUID.randomUUID(), "Gin", "123", "gin@tonic.com");
        User user2 = new User(UUID.randomUUID(), "Mario", "064", "gin1@tonic.com");

        userService.addUser(user);
        userService.addUser(user2);

        //When
        boolean userInlist = userService.getAllUsers().contains(user);

        //Then
        assertTrue(userInlist);

    }

    @Test
    public void getUser() {
        //Given
        User user = new User(UUID.randomUUID(), "Gin", "123", "gin@tonic.com");
        userService.addUser(user);

        //When
        User userName = userService.getUser(user.getUserName());

        //Then
        assertEquals(userName.getUserName(), "Gin");

    }

    @Test
    public void getUserShouldReturnException() {
        //Given //When //Then
        assertThrows(DataNotFoundException.class, () -> userService.getUser("Gino"));

    }

    @Test
    public void getUserReward() {
        //Given
        User user = new User(UUID.randomUUID(), "Gino", "123", "gin@tonic.com");
        userService.addUser(user);

        //When
        List<UserReward> userReward = userService.getUserRewards(user);

        //Then
        assertNotNull(userReward);
    }


    @Test
    public void getUserPreferences() {
        //Given
        User user = new User(UUID.randomUUID(), "Gino", "123", "gin@tonic.com");
        userService.addUser(user);

        //When
        UserPreferencesDto userPreferencesDto = userService.getUserPreferences(user.getUserName());

        //Then
        assertNotNull(userPreferencesDto);
    }

    @Test
    public void deleteAllUser() {
        //Given
        List<User> users = userService.getAllUsers();

        //When
        userService.deleteAllUser();

        //Then
        assertNotNull(users);
    }



}