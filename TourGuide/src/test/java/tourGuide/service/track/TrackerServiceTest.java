package tourGuide.service.track;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.*;
import tourGuide.service.impl.TourGuideService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TrackerServiceTest {
    @Autowired
    TrackerService trackerService;
    @Autowired
    ITourGuideService tourGuideService;

    @Autowired
    IUserService userService;

    @Autowired
    IGpsUtilService gpsUtil;
    @Autowired
    IRewardsService rewardsService;
    @Autowired
    ITripPricerService tripPricerService;


    @BeforeEach
    void init(){
        userService.deleteAllUser();
        InternalTestHelper.setInternalUserNumber(100);
        tourGuideService = new TourGuideService(gpsUtil, rewardsService, tripPricerService, userService );


    }


    @Test
    void testRun() throws InterruptedException {
       //Given
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        //When
        trackerService.start();
        trackerService.stopTracking();
        stopWatch.stop();

        //Then
        assertTrue(trackerService.isAlive());
    }
}