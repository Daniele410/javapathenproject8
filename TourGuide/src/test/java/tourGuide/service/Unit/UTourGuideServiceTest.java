package tourGuide.service.Unit;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.dto.UserPreferencesDto;
import tourGuide.model.user.User;
import tourGuide.model.user.UserPreferences;
import tourGuide.model.user.UserReward;
import tourGuide.service.*;
import tourGuide.service.impl.TourGuideService;
import tripPricer.Provider;

import java.util.*;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class UTourGuideServiceTest {

    private ITourGuideService tourGuideService;
    @MockBean
    private IRewardsService rewardsService;
    @MockBean
    private IUserService userService;
    @MockBean
    private IGpsUtilService gpsUtilService;
    @MockBean
    private ITripPricerService tripPricerService;

    @MockBean
    User user;
    @MockBean
    UserPreferencesDto userPreferencesDto;
    @MockBean
    Location location;
    @MockBean
    VisitedLocation visitedLocation;
    @MockBean
    Attraction attraction;
    @MockBean
    UserReward userReward;
    @MockBean
    Provider provider;


    @BeforeEach
    public void init() {
        userService.deleteAllUser();
        InternalTestHelper.setInternalUserNumber(100);
        tourGuideService = new TourGuideService(gpsUtilService, rewardsService, tripPricerService, userService);
        Locale.setDefault(new Locale.Builder().setLanguage("en").setRegion("US").build());

    }

    @Test
    public void getTripDeals() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        user.setUserPreferences(new UserPreferences());
        List<Provider> providers = new ArrayList<>();
        providers.add(provider);


        when(tripPricerService.getPrice(anyString(), any(UUID.class), anyInt(),
                anyInt(), anyInt(), anyInt())).thenReturn(providers);

        //When
        List<Provider> actualProviders = tourGuideService.getTripDeals(user);

        //Then
        assertEquals(providers, actualProviders);
        assertNotNull(provider);

    }


    @Test
    public void trackUserLocation() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        CompletableFuture<VisitedLocation> visitedLocation = new CompletableFuture<>();
        when(gpsUtilService.getUserLocation(any(UUID.class))).thenReturn(visitedLocation);
        //When
        CompletableFuture<VisitedLocation> location = tourGuideService.trackUserLocation(user);

        //Then
        assertNotNull(location);
    }

    @Test
    public void getUserLocation() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        List<VisitedLocation> visitedLocation1 = Collections.singletonList(new VisitedLocation(UUID.randomUUID(), location, new Date()));
        user.setVisitedLocations(visitedLocation1);
        when(userService.getUser(anyString())).thenReturn(user);


        //When
        CompletableFuture<VisitedLocation> location1 = tourGuideService.getUserLocation(user);

        //Then
        assertNotNull(location1);
    }

    @Test
    public void getAllCurrentLocations() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        List<VisitedLocation> visitedLocation1 = Collections.singletonList(new VisitedLocation(UUID.randomUUID(), location, new Date()));
        user.setVisitedLocations(visitedLocation1);
        List<User> allUsers = new ArrayList<>();
        allUsers.add(user);
        when(userService.getAllUsers()).thenReturn(allUsers);


        //When
        Map<String, Location> allLocation = tourGuideService.getAllCurrentLocations();

        //Then
        assertNotNull(allLocation);
        assertEquals(1, allLocation.size());
    }


//    @Test
//    public void updatePreferences() {
//        //Given
//        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
//        when(userService.getUser(user.getUserName())).thenReturn(user);
//        UserPreferencesDto userPreferenceDto = new UserPreferencesDto(50, 100, 500, 20, 3, 2, 3);
//        UserPreferences preferences = user.getUserPreferences();
//        preferences.setAttractionProximity(userPreferenceDto.getAttractionProximity());
//        preferences.setHighPricePoint(Money.of(userPreferenceDto.getHighPricePoint(), preferences.getCurrency()));
//        preferences.setLowerPricePoint(Money.of(userPreferenceDto.getLowerPricePoint(), preferences.getCurrency()));
//        preferences.setTripDuration(userPreferenceDto.getTripDuration());
//        preferences.setTicketQuantity(userPreferenceDto.getTicketQuantity());
//        preferences.setNumberOfAdults(userPreferenceDto.getNumberOfAdults());
//        preferences.setNumberOfChildren(userPreferenceDto.getNumberOfChildren());
//
//        //When
//        UserPreferencesDto preferencesDto = tourGuideService.updatePreferences(user.getUserName(), userPreferenceDto);
//
//        //Then
////        assertEquals(preferencesDto.getTicketQuantity(), user.getUserPreferences().getTicketQuantity());
//        assertNotNull(preferencesDto);
//    }


}