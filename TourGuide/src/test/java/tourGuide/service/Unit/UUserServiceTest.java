package tourGuide.service.Unit;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tourGuide.exception.DataNotFoundException;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.repository.UserRepository;
import tourGuide.service.IGpsUtilService;
import tourGuide.service.IRewardsService;
import tourGuide.service.ITourGuideService;
import tourGuide.service.ITripPricerService;
import tourGuide.service.impl.UserService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class UUserServiceTest {

    @InjectMocks
    private UserService userService;
    @Mock
    private UserRepository userRepository;


    @MockBean
    private ITourGuideService tourGuideService;

    @MockBean
    private IGpsUtilService gpsUtilService;
    @MockBean
    private ITripPricerService tripPricerService;

    @MockBean
    private IRewardsService rewardsService;


    @Test
    public void getUserRewardsShouldReturnReward() {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        Location location = new Location(45.817595D, -120.922008D);
        VisitedLocation visitedLocation = new VisitedLocation(UUID.randomUUID(), location, new Date());
        Attraction attraction = new Attraction("Buffalo National River", "St Joe", "AR", 35.985512, -92.757652);
        List<UserReward> userReward = Collections.singletonList(new UserReward(visitedLocation, attraction, 22));
        user.setUserRewards(userReward);
        when(userRepository.getUserByUserName(user.getUserName())).thenReturn(user);

        // When
        List<UserReward> rewards = userService.getUserRewards(user);

        // Then
        assertNotNull(rewards);
        assertEquals(rewards.get(0).getRewardPoints(), 22);
    }

    @Test
    public void testGetUserShouldReturnUser() {
        // Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        when(userRepository.getUserByUserName("Mario")).thenReturn(user);

        // When
        User resultUser = userService.getUser("Mario");

        //Then
        assertEquals(user, resultUser);

    }

    @Test
    public void testGetUserShouldReturnException() throws DataNotFoundException {
        // Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        when(userRepository.getUserByUserName("Mario")).thenReturn(null);


        //When  //Then
        assertThrows(DataNotFoundException.class, () -> userService.getUser(user.getUserName()));

    }

    @Test
    public void testGetAllUsersShouldReturnUsers() {
        // Given
        List<User> users = new ArrayList<>();
        when(userRepository.getAllUsers()).thenReturn(users);

        // When
        List<User> resultUsers = userService.getAllUsers();

        //Then
        assertEquals(users, resultUsers);

    }

    @Test
    public void testAddUserShouldReturnUser() {
        // Given
        User user = new User(UUID.randomUUID(), "Gin", "123", "gin@tonic.com");
        when(userRepository.getAllUsers()).thenReturn(Collections.emptyList());

        //When
        userService.addUser(user);

        //Then
        verify(userRepository).addUser("Gin", user);
    }

    @Test
    public void testAddExistingUser() throws DataNotFoundException {
       //Given
        User user = new User(UUID.randomUUID(), "Gin", "123", "gin@tonic.com");
        when(userRepository.getAllUsers()).thenReturn(Collections.singletonList(user));

        //When  //Then
        assertThrows(DataNotFoundException.class, () -> userService.addUser(user));

    }

}








