package tourGuide.service.Unit;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import rewardCentral.RewardCentral;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.service.impl.RewardsService;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class URewardsServiceTest {

    private IRewardsService rewardService;
    @MockBean
    private RewardCentral rewardCentral;

    @MockBean
    private IGpsUtilService gpsUtil;

    @MockBean
    private ExecutorService executorService;
    @MockBean
    ITourGuideService tourGuideService;
    @MockBean
    private ITripPricerService tripPricerService;

    @MockBean
    private IUserService userService;
    @MockBean
    User user;

    @MockBean
    UserRepository userRepository;


    @BeforeEach
    public void setUp() {
        rewardService = new RewardsService(gpsUtil, rewardCentral);

    }


    @Test
    void calculateRewardsTest() throws ExecutionException, InterruptedException {
        //Given
        User user = new User(UUID.randomUUID(), "Mario", "000", "mario@bros.com");
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), new Location(1.0, 2.0), null);
        user.addToVisitedLocations(visitedLocation);
        Attraction attraction = new Attraction("test1", "test1", "test1", 1.0, 2.0);
        when(gpsUtil.getAttractions()).thenReturn(List.of(attraction));

        //When
        rewardService.calculateRewards(user);

        //Then
        Mockito.verify(gpsUtil).getAttractions();

    }



}