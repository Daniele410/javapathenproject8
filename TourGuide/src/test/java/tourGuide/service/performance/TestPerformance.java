package tourGuide.service.performance;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tourGuide.model.user.User;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.*;
import tourGuide.service.impl.TourGuideService;
import tourGuide.service.track.ITrackerService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class TestPerformance {

    @Autowired
    IUserService userService;
    @Autowired
    IGpsUtilService gpsUtil;
    @Autowired
    IRewardsService rewardsService;
    @Autowired
    ITripPricerService tripPricerService;
    @Autowired
    ITourGuideService tourGuideService;

    private StopWatch stopWatch;
    @Autowired
    ITrackerService tracker;

    @BeforeEach
    public void init() {
        userService.deleteAllUser();
        stopWatch = new StopWatch();
        Locale.setDefault(new Locale.Builder().setLanguage("en").setRegion("US").build());
        InternalTestHelper.setInternalUserNumber(100000);
        tourGuideService = new TourGuideService(gpsUtil, rewardsService, tripPricerService, userService);

    }

    /*
     * A note on performance improvements:
     *
     *     The number of users generated for the high volume tests can be easily adjusted via this method:
     *
     *     		InternalTestHelper.setInternalUserNumber(100000);
     *
     *     These tests can be modified to suit new solutions, just as long as the performance metrics
     *     at the end of the tests remains consistent.
     *
     *     These are performance metrics that we are trying to hit:
     *
     *     highVolumeTrackLocation: 100,000 users within 15 minutes:
     *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
     *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     */


    @Test
    public void highVolumeTrackLocation() throws ExecutionException, InterruptedException, TimeoutException, NullPointerException {

        // Users should be incremented up to 100,000, and test finishes within 15 minutes
        List<User> allUsers = userService.getAllUsers();
        List<CompletableFuture<VisitedLocation>> tasksFuture = new ArrayList<>();

        stopWatch.start();


        allUsers.parallelStream().forEach(user -> {
            tasksFuture.add(tourGuideService.trackUserLocation(user));
        });
        // CompletableFuture.allOf :wait for all futures to complete, with a 15 minutes timeout
        CompletableFuture<Void> allFutures = CompletableFuture
                .allOf(tasksFuture.toArray(new CompletableFuture[0]));

        allFutures.get(15, TimeUnit.MINUTES);

        stopWatch.stop();
        tracker.stopTracking();
        assertTrue(allFutures.isDone());
        System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
    }


    @Test
    public void highVolumeGetRewards() throws  NullPointerException {

        // Users should be incremented up to 100,000, and test finishes within 20 minutes
        Attraction attraction = gpsUtil.getAttractions().get(0);

        List<User> allUsers = userService.getAllUsers();
        for (User users : allUsers) {
            users.addToVisitedLocations(new VisitedLocation(users.getUserId(), attraction, new Date()));
        }

        List<CompletableFuture<Void>> tasksFuture = new ArrayList<>();
        for (User user : allUsers) {
            tasksFuture.add(rewardsService.calculateRewards(user));
        }

        stopWatch.start();

        CompletableFuture<Void> allFutures = CompletableFuture
                .allOf(tasksFuture.toArray(new CompletableFuture[0]));

        allFutures.join();

        System.out.println(allUsers.size());
        allUsers.parallelStream().forEach(user -> {
            assertNotEquals(0, user.getUserRewards().get(0).getRewardPoints());
            assertTrue(user.getUserRewards().size() > 0);
        });

        stopWatch.stop();
        tracker.stopTracking();
        assertTrue(allFutures.isDone());
        System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch
                .getTime()));

    }

}
