# TourGuide

## About The Project

TourGuide is a Spring Boot application and a core part of the TripMaster
application portfolio. It allows users to search for discounts on nearby
attractions and accommodations as well as tickets to various shows.

---------
## Technical:
1. Java 19
2. Framework: Spring Boot v3.0.2
3. Gradle 7.6
4. JUnit 5
5. Jacoco 0.8.8
6. GitLab

---------

## Getting Started

### Running App

1. To run the application, go to folder `TourGuide`

2. Compile and generate the final jar by running command line: `gradle build`

After for start application digit

Command line : `gradle bootRun`

1. To access the application, open your favorite browser and go to address:
- The app generates 100 internal user for test with name example:"internalUser23". Add name for test end Point.

* **GET** = http://localhost:8080/
* **GET** = http://localhost:8080/getAllUser
* **GET** = http://localhost:8080/getUser?userName= "internalUser76"
* **GET** = http://localhost:8080/getLocation?userName= "internalUser78"
* **GET** = http://localhost:8080/getNearByAttractions?userName= "internalUser74"
* **GET** = http://localhost:8080/getRewards?userName= "internalUser78"
* **GET** = http://localhost:8080/getNearByAttractions?userName= "internalUser23"
* **GET** = http://localhost:8080/getRewards?userName= "internalUser23"
* **GET** = http://localhost:8080/getAllCurrentLocations 
* **GET** = http://localhost:8080/getTripDeals?userName= "internalUser23"
* **GET** = http://localhost:8080/userPreference?userName= "internalUser23"
* **PUT** = http://localhost:8080/userPreference?userName= "internalUser23"


### Testing
- To run just the tests execute the command: `gradle test`